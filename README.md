# Cruxified Clojars stats

This project let's you play around with Crux using the [daily Clojars stats](https://clojars.org/stats/).

## setup

Clone it.

## usage

``` shell
# get latest database
$ ./bin/fetch-db

# ...then play around using rebel
$ ./bin/cli rebel
[Rebel readline] Type :repl/help for online help info
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
Crux-node available as var 'node'.
clojars-stats-crux.main=> (crux/q (crux/db node) '{:find [e] :where [[e :library/group "clj-time"]] :full-results? true})
#{[{:crux.db/id :clj-time/clj-time,
    :library/group "clj-time",
    :library/artifact "clj-time",
    :library/daily-downloads
    {"0.7.0" 11,
     "0.14.4" 563,
     "0.6.0" 164,
     "0.15.0" 1155,
     "0.10.0" 105,
     "0.3.7" 17,
     "0.14.3" 1602,
     "0.12.1" 15,
     "0.8.0" 702,
     "0.4.1" 1960,
     "0.4.4" 116,
     "0.9.0" 148,
     "0.5.0" 6,
     "0.15.1" 828,
     "0.13.0" 1002,
     "0.11.0" 2190,
     "0.12.2" 175,
     "0.14.0" 414,
     "0.5.1" 5,
     "0.3.0" 1,
     "0.15.2" 522,
     "0.12.0" 233,
     "0.14.2" 299}}]}
clojars-stats-crux.main=> (crux/history node :clj-time/clj-time)
[{:crux.db/id "f8e355da737cbc3a2adfa39713b0e9c40b3d0588",
  :crux.db/content-hash "f6fc55a5f7797da545dea399b525ba50dd55412d",
  :crux.db/valid-time #inst "2019-11-04T00:00:00.000-00:00",
  :crux.tx/tx-time #inst "2019-11-05T15:26:49.935-00:00",
  :crux.tx/tx-id 3336}]
clojars-stats-crux.main=>
```
