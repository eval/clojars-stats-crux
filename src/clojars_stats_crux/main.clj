(ns clojars-stats-crux.main
  "Cruxified Clojars stats

Ensure you have a local db (stats.sqlite) via `./bin/fetch-db`,
then play around via `cli rebel`."
  #:inclined{:option.db {:desc "sqlite-database"
                         :default "stats.sqlite"}}
  (:require [clojars-stats-crux.repl :refer (syntax-highlight-pprint)]
            [clojure.edn :as edn]
            [clojure.instant :as instant]
            [clojure.string :as str]
            [crux.api :as crux]
            [rebel-readline.clojure.main :as rebel-main]
            [rebel-readline.core :as rebel-core]
            [tick.alpha.api :as t])
  (:import (crux.api ICruxAPI)
           (java.time Duration)
           (java.util UUID)))


(defn ^:private node-with-dbname [dbname]
  (let [options {:crux.node/topology :crux.jdbc/topology
                 :crux.node/kv-store "crux.kv.memdb/kv"
                 :crux.jdbc/dbtype   "sqlite"
                 :crux.jdbc/dbname   dbname}]
    (crux/start-node options)))


(defn ^:private filename->date [filename]
  (->> filename
       (re-find #"(\d{4})(\d{2})(\d{2})")
       rest
       (str/join "-")
       (t/date)))


(defn ^#:inclined{:option.stats-file! {:desc "file with clojars-stats (name like foo-YYYYMMdd.edn)"}}
  ingest
  "Put data from stats-file in crux-db"
  [{:keys [global/db stats-file]}]
  (let [stats            (-> stats-file slurp (edn/read-string))
        node             (node-with-dbname db)
        valid-time-start (-> stats-file (filename->date)
                             (t/at (t/time "00:00"))
                             (t/in "UTC"))
        valid-time-end   (t/+ valid-time-start (t/new-period 1 :days))
        puts             (into []
                               (for [[[group artifact] version-stats] stats
                                     :let                             [library-id (keyword (str group "/" artifact))]]
                                 [:crux.tx/put
                                  #:library{:crux.db/id      library-id
                                            :group           group
                                            :artifact        artifact
                                            :daily-downloads version-stats}
                                  (t/inst valid-time-start)
                                  (t/inst valid-time-end)]))]
    (crux/submit-tx node puts)))


(defn ^#:inclined{}
  rebel
  "Open a rebel-readline."
  [{:keys [global/db]}]
  (rebel-core/ensure-terminal
   (rebel-main/repl
    :print syntax-highlight-pprint
    :init (fn []
            ;; otherwise inspecting node-var will make repl unresponsive
            (set! *print-length* 50)
            (in-ns 'clojars-stats-crux.main)
            (def ^:private node (node-with-dbname db))
            (println "Crux-node available as var 'node'.")
            ))))


(comment

  )
